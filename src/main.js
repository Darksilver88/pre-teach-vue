import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'

import {LoadingPlugin} from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/css/index.css';

let app = createApp(App);
app.use(LoadingPlugin);
app.mount('#app')

//createApp(App).mount('#app')
