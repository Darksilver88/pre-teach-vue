import fs from 'fs';

fs.readFile('./dist/index.html', 'utf8', (err, data) => {
    if (err) {
        console.error(`Error reading file: ${err.message}`);
        return;
    }

    const modifiedContent = data
        .replace(new RegExp("/assets/", "g"), "./assets/")
        .replace(new RegExp("/favicon.ico", "g"), "./favicon.ico");

    fs.writeFile('./dist/index.html', modifiedContent, 'utf8', (err) => {
        if (err) {
            console.error(`Error writing file: ${err.message}`);
        } else {
            console.log('File modified successfully.');
        }
    });
});


fs.readdir('./dist/assets/', (err, files) => {
    if (err) {
        console.error('Error reading folder:', err);
        return;
    }

    console.log('Files in the folder:');
    files.forEach((file) => {
        let filePath = './dist/assets/' + file;
        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
                console.error(`Error reading file: ${err.message}`);
                return;
            }

            if (!file.includes(".js") && !file.includes(".css")) {
                return;
            }

            let modifiedContent;
            if (file.includes(".js")) {
                modifiedContent = data
                    .replace(new RegExp("/assets/", "g"), "./assets/");
            }

            if (file.includes(".css")) {
                  modifiedContent = data
                     .replace(new RegExp("/assets/", "g"), "./"); 
            }

            if (modifiedContent == undefined) {
                return;
            }


            fs.writeFile(filePath, modifiedContent, 'utf8', (err) => {
                if (err) {
                    console.error(`Error writing file: ${err.message}`);
                } else {
                    console.log('File modified successfully.');
                }
            });
        });

    });
});


